package com.intertech;

import com.intertech.dao.TeamDao;
import com.intertech.dao.TeamDaoHibernateImpl;
import com.intertech.domain.FamousFan;
import com.intertech.domain.Team;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Team team = new Team();
		FamousFan fan = new FamousFan();
		team.setMascot("Duck");
		team.setNickname("Ducks");
		fan.setFirstName("Billy");
		fan.setLastName("Bob");
		team.addFan(fan);
		TeamDao dao = new TeamDaoHibernateImpl();
		dao.saveTeam(team);

	}

}
