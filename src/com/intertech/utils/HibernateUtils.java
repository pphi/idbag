package com.intertech.utils;

//import org.hibernate.HibernateException;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;
//import org.hibernate.service.ServiceRegistry;
//import org.hibernate.service.ServiceRegistryBuilder;
//
//public class HibernateUtils 
//{
//	private static SessionFactory sf;
//	private static ServiceRegistry sr;
//	
//	static 
//	{
//		resetSessionFactory();
//	}
//
//	public static SessionFactory getSessionFactory() 
//	{
//		return sf;
//	}
//
//	public static void resetSessionFactory() throws HibernateException 
//	{
//	    Configuration configuration = new Configuration();
//	    configuration.configure();
//	    sr = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
//	    sf = configuration.buildSessionFactory(sr);
//	}
//
//	public static Session getSession() 
//	{
//		return sf.openSession();
//	}
//}


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	private static SessionFactory sf;

	static {
		resetSessionFactory();
	}

	public static SessionFactory getSessionFactory() {
		return sf;
	}

	public static void resetSessionFactory() {
		//sf = new Configuration().buildSessionFactory();
		sf = new Configuration().configure().buildSessionFactory();
	}

	public static Session getSession() {
		return sf.openSession();
	}
}