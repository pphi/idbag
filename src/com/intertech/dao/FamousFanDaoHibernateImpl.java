package com.intertech.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.intertech.domain.FamousFan;
import com.intertech.utils.HibernateUtils;

public class FamousFanDaoHibernateImpl implements FamousFanDao {

	@Override
	public void saveFamousFan(FamousFan record) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		session.save(record);
		t.commit();
		session.close();

	}

	@Override
	public void updateFamousFan(FamousFan record) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		session.update(record);
		t.commit();
		session.close();

	}

	@Override
	public void deleteFamousFan(Long id) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		FamousFan record = (FamousFan) session.get(FamousFan.class, id);
		session.delete(record);
		t.commit();
		session.close();

	}

	@Override
	public List<FamousFan> getFamousFan() {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		Query q = session.createQuery("from FamousFan");
		@SuppressWarnings("unchecked")
		List<FamousFan> results = q.list();
		t.commit();
		session.close();
		return results;
	}

	@Override
	public FamousFan getFamousFans(Long id) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		FamousFan record = (FamousFan) session.get(FamousFan.class, id);
		t.commit();
		session.close();
		return record;
	}
}
