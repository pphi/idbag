package com.intertech.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.intertech.domain.Team;
import com.intertech.utils.HibernateUtils;

public class TeamDaoHibernateImpl implements TeamDao {

	@Override
	public void saveTeam(Team record) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		session.save(record);
		t.commit();
		session.close();
	}

	@Override
	public void updateTeam(Team record) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		session.update(record);
		t.commit();
		session.close();

	}

	@Override
	public void deleteTeam(Long id) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		Team record = (Team) session.get(Team.class, id);
		session.delete(record);
		t.commit();
		session.close();

	}

	@Override
	public List<Team> getTeam() {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		Query q = session.createQuery("from Team");
		@SuppressWarnings("unchecked")
		List<Team> results = q.list();
		t.commit();
		session.close();
		return results;
	}

	@Override
	public Team getTeams(Long id) {
		Session session = HibernateUtils.getSession();
		Transaction t = session.beginTransaction();
		Team record = (Team) session.get(Team.class, id);
		t.commit();
		session.close();
		return record;
	}

}
