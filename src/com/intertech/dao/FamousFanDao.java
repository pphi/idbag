package com.intertech.dao;

import java.util.List;

import com.intertech.domain.FamousFan;

public interface FamousFanDao {

	public void saveFamousFan(FamousFan record);

	public void updateFamousFan(FamousFan record);

	public void deleteFamousFan(Long id);

	public List<FamousFan> getFamousFan();

	public FamousFan getFamousFans(Long id);
}
