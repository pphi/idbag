package com.intertech.dao;

import java.util.List;

import com.intertech.domain.Team;

public interface TeamDao {

	public void saveTeam(Team record);

	public void updateTeam(Team record);

	public void deleteTeam(Long id);

	public List<Team> getTeam();

	public Team getTeams(Long id);
}
