package com.intertech.domain;

import java.util.ArrayList;
import java.util.Collection;

public class Team {

	private long id;
	private String nickname;
	private String mascot;
	private Collection<FamousFan> fans = new ArrayList<FamousFan>();
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getMascot() {
		return mascot;
	}
	public void setMascot(String mascot) {
		this.mascot = mascot;
	}
	public Collection<FamousFan> getFans() {
		return fans;
	}
	public void setFans(Collection<FamousFan> fans) {
		this.fans = fans;
	}
	
	public void addFan(FamousFan aFan) {
		fans.add(aFan);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
