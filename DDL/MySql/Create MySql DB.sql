SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `idBagTest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `idBagTest` ;

-- -----------------------------------------------------
-- Table `idBagTest`.`Team`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `idBagTest`.`Team` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `nickname` VARCHAR(45) NULL ,
  `mascot` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `idBagTest`.`Famous_Fan`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `idBagTest`.`Famous_Fan` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(45) NULL ,
  `last_name` VARCHAR(45) NULL ,
  `description` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `idBagTest`.`Famous_Fan_Team`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `idBagTest`.`Famous_Fan_Team` (
  `Team_id` INT NOT NULL ,
  `Famous_Fan_id` INT NOT NULL ,
  `team_fan_id` INT NOT NULL AUTO_INCREMENT ,
  INDEX `fk_Team_has_Famous_Fan_Famous_Fan1_idx` (`Famous_Fan_id` ASC) ,
  INDEX `fk_Team_has_Famous_Fan_Team_idx` (`Team_id` ASC) ,
  PRIMARY KEY (`team_fan_id`) ,
  CONSTRAINT `fk_Team_has_Famous_Fan_Team`
    FOREIGN KEY (`Team_id` )
    REFERENCES `idBagTest`.`Team` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Team_has_Famous_Fan_Famous_Fan1`
    FOREIGN KEY (`Famous_Fan_id` )
    REFERENCES `idBagTest`.`Famous_Fan` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
